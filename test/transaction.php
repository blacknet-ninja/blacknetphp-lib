<?php

namespace Blacknet\Test\Lib;

use Blacknet\Lib\Core\Serialize;
use Blacknet\Lib\Core\JSONRPC;

use PHPUnit\Framework\TestCase;

const testAccount    = "blacknet14w6tm25y7rt24zj7r8fq7rnzd50qtpgmpfwv50r7qjnqhcwlxszqanh036";
const testMnemonic   = "piano maze provide discover tower scissors true leave senior aware secret film";
const testPkHex      = "abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df3404";
const testSkHex      = "1e662e8fc3df898cc86777af5b3711c5115ed49c302d47fcb164cea5f18ea2c7abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df3404";
const testMessage    = "BLN-is-very-nice";
const testSign       = "FF6D74C0493720F59DA4F06CD6D13D4A47D04F61E6D4AFF3D001EBD59153DC6DF40DDA34F7CD63FBA76CD2C1CA3963D1CB4F17F2061FB191BA441F0BF925C40B";
const testSerialized = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000abb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080";
const testSignature  = "7cad618727b1dd3872d685c4c2eba86843e64321fe622a60a66807e5a495a7b081a04eb8eebd84b683d2cbad8b13ce6277b4e2280ec6da528d0b9f344f246a0babb4bdaa84f0d6aa8a5e19d20f0e626d1e05851b0a5cca3c7e04a60be1df34040000000098142b000347fb0fb10c15c6136b759af8963c46d7bcd74f1bbc18ca22dcec1700000000000186a000aa000000003b9aca001c6c6e21ce7d9a16892753d801b778549692aa23f0655ba83a5c46e7475cb5f30080";
const testEncrypted  = "669E238CBD3977550DEAB18DEE256BCB16748CA9";
const testStr        = "blacknet";
const testMnemonic2  = "prepare long erode easy moment dinosaur soft sound exhibit wire mesh muffin";
const testAccount2   = "blacknet1pns3tp3wja8jmqqyfy0pvqedg9nv4zj3tkcpqmep5hr3rvw2rnkqhafpf9";

class TransactionTest extends TestCase
{
    public function testTransfer()
    {
        $url = "https://blnmobiledaemon.blnscan.io";
        $jsonrpc = new JSONRPC($url);
        $body = $jsonrpc->transfer(array(
            "from"=>testAccount2,
            "to"=>testAccount2,
            "message"=>"xxxx",
            "fee"=>0.001*1e8,
            "amount"=>1e8,
            "mnemonic"=>testMnemonic2
        ));
        $this->assertEquals(200, $body->code);
    }
    public function testLease()
    {
        $s = new JSONRPC();
        $body = $s->lease(array(
            "from"=>testAccount2,
            "to"=>testAccount2,
            "fee"=>0.001*1e8,
            "amount"=>1e8,
            "mnemonic"=>testMnemonic2
        ));
        $this->assertEquals('Transaction rejected: 100000000 less than minimal 100000000000', $body->body);
    }
    public function testCancelLease()
    {
        $s = new JSONRPC();
        $body = $s->cancelLease(array(
            "from"=>testAccount2,
            "to"=>testAccount2,
            "fee"=>0.001*1e8,
            "amount"=>1e8,
            "height"=>1111111,
            "mnemonic"=>testMnemonic2
        ));
        $this->assertEquals('Transaction rejected: Lease not found', $body->body);
    }
    public function testWithdrawFromLease()
    {
        $s = new JSONRPC();
        $body = $s->withdrawFromLease(array(
            "withdraw"=>1e8,
            "from"=>testAccount2,
            "to"=>testAccount2,
            "fee"=>0.001*1e8,
            "amount"=>1e8,
            "height"=>1111111,
            "mnemonic"=>testMnemonic2
        ));
        $this->assertEquals('Transaction rejected: Can not withdraw more than -99900000000', $body->body);
    }
}