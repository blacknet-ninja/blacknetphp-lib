<?php

namespace Blacknet\Lib\Core;

use ParagonIE_Sodium_Core_ChaCha20;
use ParagonIE_Sodium_Core_ChaCha20_IetfCtx;
use ParagonIE_Sodium_Core32_ChaCha20;
use ParagonIE_Sodium_Core32_ChaCha20_IetfCtx;

class Chacha20
{
    public static function encrypt($key, $nonce, $message){
        if (PHP_INT_SIZE === 4) {
            return Chacha2032::encrypt($key, $nonce, $message);
        } else {
            return Chacha2064::encrypt($key, $nonce, $message);
        }
    }
    public static function decrypt($key, $nonce, $message){
        if (PHP_INT_SIZE === 4) {
            return Chacha2032::decrypt($key, $nonce, $message);
        } else {
            return Chacha2064::decrypt($key, $nonce, $message);
        }
    }
}

class Chacha2032
{
    public static function encrypt($key, $nonce, $message){
        return ParagonIE_Sodium_Core32_ChaCha20::encryptBytes(
            new ParagonIE_Sodium_Core32_ChaCha20_IetfCtx($key, $nonce),
            $message
        );
    }
    public static function decrypt($key, $nonce, $message){
        return ParagonIE_Sodium_Core32_ChaCha20::encryptBytes(
            new ParagonIE_Sodium_Core32_ChaCha20_IetfCtx($key, $nonce),
            $message
        );
    }
}

class Chacha2064
{
    public static function encrypt($key, $nonce, $message){
        return ParagonIE_Sodium_Core_ChaCha20::encryptBytes(
            new ParagonIE_Sodium_Core_ChaCha20_IetfCtx($key, $nonce),
            $message
        );
    }
    public static function decrypt($key, $nonce, $message){
        return ParagonIE_Sodium_Core_ChaCha20::encryptBytes(
            new ParagonIE_Sodium_Core_ChaCha20_IetfCtx($key, $nonce),
            $message
        );
    }
}