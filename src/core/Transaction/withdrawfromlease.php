<?php

namespace Blacknet\Lib\Core\Transaction;
use Blacknet\Lib\Core\Utils;

class WithdrawFromLease extends Utils{
    var $withdraw;//uint64
    var $amount; //uint64
    var $to; //string
    var $height; //uint32
    function __construct($withdraw, $amount, $to, $height) {
        $this->withdraw= intval($withdraw);
        $this->amount  = intval($amount);
        $this->to      = self::publickeyToHex(self::publickey($to));
        $this->height  = intval($height);
    }
    public function serialize(){
        $withdraw = self::toUint64Array($this->withdraw);
        $amount   = self::toUint64Array($this->amount);
        $to       = self::stringToArray(self::hexToPublickey($this->to));
        $height   = self::toUint32Array($this->height);
        return array_merge(
            $withdraw,
            $amount,
            $to,
            $height
        );
    }
    public static function derialize(array $arr){
        $withdraw= self::uint64ArrayToNumeric(array_slice($arr, 0, 8));
        $amount  = self::uint64ArrayToNumeric(array_slice($arr, 8, 8));
        $to      = self::publickeyToHex(self::arrayToString(array_slice($arr, 16, 32)));
        $height  = self::uint32ArrayToNumeric(array_slice($arr, 48));
        return new CancelLease($amount, $to, $height);
    }
}
